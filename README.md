#  fuchain-sdk-java 使用文档

## 介绍

福链科技有限公司开发的fulian联盟链是基于fabric2.2.1版本研发的。本版本sdk提供全套面向底层接口，方便用户在此基础上进行二次开发。 适用范围 基于fabric2.2.1版的fulian联盟链。

##  配置文件说明  
  与fuchain链低层交互配置文件：application-flkj.yaml

    ```
        flkj:
		  cc: org1 #  #org的标识（确保唯一性）
		  orgMSPID: Org1MSP
		  openTLS: true

		  #user
		  userleagueName: example #  联盟名称
		  userorgName: org1
		  userpeerName: peer0 # 连接peer节点name
		  username: org1Peer0Admin
		  userskPath: crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp/keystore/priv_sk
		  usercertificatePath: crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp/signcerts/Admin@org1.example.com-cert.pem


		  #orderer
		  orderername: orderer
		  ordererlocation: orderer.example.com:7050
		  ordererserverCrtPath: crypto-config/ordererOrganizations/example.com/orderers/orderer.example.com/tls/ca.crt
		  ordererclientCertPath: crypto-config/ordererOrganizations/example.com/orderers/orderer.example.com/tls/server.crt
		  ordererclientKeyPath: crypto-config/ordererOrganizations/example.com/orderers/orderer.example.com/tls/server.key

		  #peer
		  peerName: peer0
		  peerLocation: peer0.org1.example.com:7051
		  peerEventHubLocation: peer0.org1.example.com:7051
		  peerserverCrtPath: crypto-config/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt
		  peerclientCertPath: crypto-config/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/server.crt
		  peerclientKeyPath: crypto-config/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/server.key

		  #channel
		  channelName: mychannel

    ```
##  pom文件加入应用

    pom文件引入jar包,本地maven仓库导入fuchain-sdk-java-221rc-2.2.1.jar
    ```xml
        <dependency>
            <groupId>fuchain-sdk-java</groupId>
            <artifactId>fuchain-sdk-java-221rc</artifactId>
            <version>2.2.1</version>
        </dependency>
    ```
	
## 初始化方法及调用   

springboot初始化   

```
package cn.flkj.lucia1a4a1.config;


import cn.flkj.lucia1a4a1.intermediate.OrgManager;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.hyperledger.fabric.protos.peer.Query;
import org.hyperledger.fabric.sdk.Peer;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;


/**
 * @author : 州长在手 2020/9/17 下午7:56
 */
@Component
@ConfigurationProperties(prefix = "flkj")
@Data
@Slf4j
public class Runner {

    /*init*/
    private String cc;
    private String orgMSPID;

    private boolean openTLS;
    /*setUser*/
    private String userleagueName;
    private String userorgName;
    private String userpeerName;
    private String username;
    private String userskPath;
    private String usercertificatePath;
    /*addOrderer*/
    private String orderername;
    private String ordererlocation;
    private String ordererserverCrtPath;
    private String ordererclientCertPath;
    private String ordererclientKeyPath;
    /*addPeer*/
    private String peerName;
    private String peerLocation;
    private String peerEventHubLocation;
    private String peerserverCrtPath;
    private String peerclientCertPath;
    private String peerclientKeyPath;
    /*setChannel*/
    private String channelName;


    @Bean(name = "om")
    public OrgManager orgManager() {

        try {

            OrgManager om = new OrgManager(
                    getOrgMSPID(),
                    isOpenTLS());

            om.addOrderer(getOrderername(),
                    getOrdererlocation(),
                    getOrdererserverCrtPath(),
                    getOrdererclientCertPath(),
                    getOrdererclientKeyPath());

            om.addPeer(getPeerName(),
                    getPeerLocation(),
                    getPeerEventHubLocation(),
                    getPeerserverCrtPath(),
                    getPeerclientCertPath(),
                    getPeerclientKeyPath());

            om.setUser(getUserleagueName(),
                    getUserorgName(),
                    getUserpeerName(),
                    getUsername(),
                    getUserskPath(),
                    getUsercertificatePath(),
                    getOrgMSPID());

            om.setChannel(getChannelName());

            om.start();

            System.out.println("----------------FM INIT OK !!!----------------");
            return om;
        } catch (Exception e) {
            log.error(e.getMessage());
            System.out.println("----------------FM INIT FAILED & EXIT----------------");
            System.exit(0);
            return null;
        }
    }
}

```
springboot使用

```
package cn.flkj.lucia1a4a1.controller;


import cn.flkj.lucia1a4a1.intermediate.OrgManager;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * @author : 州长在手 2020/9/10 上午11:52
 * 1. 测试数据加载
 */
@Slf4j
@RestController
@CrossOrigin
@RequestMapping(value = "/sub")
public class SubstrateController {

    private final OrgManager orgManager;

    public SubstrateController(OrgManager orgManager) {
        this.orgManager = orgManager;
    }


    @PostMapping("/invoke")
    public Object invoke(
            @RequestParam("chaincode") String chaincode,
            @RequestParam("fcn") String fcn,
            @RequestParam("args") String args)  {
        return orgManager.invoke(
                chaincode,
                fcn,
                JSONObject.parseArray(args).<String>toArray(new String[0]));
    }


```


## java普通类初始化

**装载配置信息 **

```
package cn.flkj;

/**
 * @author : 州长在手 2021/1/28 上午9:39
 */
 public  class ConfigInfo {

    /*init*/
    // 确定org的唯一标识符
    public String cc = "org1";
    public String orgMSPID = "Org1MSP";

    public boolean openTLS = true;
    /*setUser*/
    // 联盟名称
    public String userleagueName = "example";
    public String userorgName = "org1";
    public String userpeerName = "peer0";
    public String username = "org1Peer0Admin";
    public String userskPath = "/home/ct/workspace/crypto/chain2a2/crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp/keystore/priv_sk";
    public String usercertificatePath = "/home/ct/workspace/crypto/chain2a2/crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp/signcerts/Admin@org1.example.com-cert.pem";
    /*addOrderer*/
    public String orderername = "orderer";
    public String ordererlocation = "orderer.example.com:7050";
    public String ordererserverCrtPath = "/home/ct/workspace/crypto/chain2a2/crypto-config/ordererOrganizations/example.com/orderers/orderer.example.com/tls/ca.crt";
    public String ordererclientCertPath = "/home/ct/workspace/crypto/chain2a2/crypto-config/ordererOrganizations/example.com/orderers/orderer.example.com/tls/server.crt";
    public String ordererclientKeyPath = "/home/ct/workspace/crypto/chain2a2/crypto-config/ordererOrganizations/example.com/orderers/orderer.example.com/tls/server.key";
    /*addPeer*/
    public String peerName = "peer0";
    public String peerLocation = "peer0.org1.example.com:7051";
    public String peerEventHubLocation = "peer0.org1.example.com:7051";
    public String peerserverCrtPath = "/home/ct/workspace/crypto/chain2a2/crypto-config/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/ca.crt";
    public String peerclientCertPath = "/home/ct/workspace/crypto/chain2a2/crypto-config/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/server.crt";
    public String peerclientKeyPath = "/home/ct/workspace/crypto/chain2a2/crypto-config/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/tls/server.key";
    /*setChannel*/
    public String channelName = "mychannel";
}


```
**构造 OrgManager 对象 **

```
package cn.flkj;

import cn.flkj.intermediate.OrgManager;

/**
 * @author : 州长在手 2021/1/28 上午9:49
 */
public class OM extends OrgManager{

    private static final ConfigInfo configInfo = new ConfigInfo();

    public OM(){

        super(configInfo.orgMSPID,configInfo.openTLS);

        addOrderer(configInfo.orderername,
               configInfo.ordererlocation,
                configInfo.ordererserverCrtPath,
                configInfo.ordererclientCertPath,
                configInfo.ordererclientKeyPath);

        addPeer(configInfo.peerName,
                configInfo.peerLocation,
                configInfo.peerEventHubLocation,
                configInfo.peerserverCrtPath,
                configInfo.peerclientCertPath,
                configInfo.peerclientKeyPath);

        setUser(configInfo.userleagueName,
                configInfo.userorgName,
                configInfo.userpeerName,
                configInfo.username,
                configInfo.userskPath,
                configInfo.usercertificatePath,
                configInfo.orgMSPID);

        setChannel(configInfo.channelName);

        try {
            start();
            System.out.println("----------------FM INIT OK !!!----------------");
        }catch (Exception e) {
            System.out.println("----------------FM INIT FAILED & EXIT----------------");
            e.printStackTrace();
            System.exit(0);
        }

    }
}

```

** 简单使用 **

```

package cn.flkj;

import com.alibaba.fastjson.JSONObject;

/**
 * @author : 州长在手 2021/1/28 上午9:35
 */
public class Main {
    public static void main(String[] args) {
        OM om = new OM();
        JSONObject json = om.queryBlockByNumber(10);
        System.out.println(json);
    }
}

```

## API 示例文档

### 安装链码
------

1. ** 在对等节点安装链码 **

```
	public JSONObject lifecycleChaincodeInstall(
            String chaincodeName, //  链码名字
            String chaincodeVersion, // 链码版本
            String chaincodeLabel, // 标签，是名字加版本号的结合
            String chaincodeSourceLocation, // GOPATH
            String chaincodePath, // 相对于GOPATH的路径
            String metadadataSource, // metadadataSource,一般是couchdb的连接方式JSON的路径
            String chaincodeEndorsementPolicyPath // 策略文件
    ) 
	
	返回结果：
	
	{ "package_id":"fabcar_1:762e0fe3dbeee0f7b08fb6200adeb4a3a20f649a00f168c0b3c2257e53b6e506"}
	

	示例参数：
	chaincodeName:fabcarsdkJava
	chaincodeVersion:1
	chaincodeSourceLocation:/home/ct/go
	chaincodePath:github.com/hyperledger/fabric-samples/chaincode/fabcar/go
	metadadataSource:/home/ct/IdeaProjects/fabric-sdk-java/src/test/fixture/meta-infs/end2endit
	chaincodeEndorsementPolicyPath:/home/ct/IdeaProjects/fabric-sdk-java/src/test/fixture/sdkintegration/chaincodeendorsementpolicy.yaml
	chaincodeLabel:fabcarsdkJava_1
	
	使用：
    om.lifecycleChaincodeInstall(...)
```	

2.  ** 对等节点审批链码 **

```
   public JSONObject lifecycleChaincodeApproveformyorg(
                                                         String chaincodeName,  // 链码名字，同lifecycleChaincodeInstall
                                                         String chaincodeVersion, // 链码版本，同lifecycleChaincodeInstall
                                                         String packageid) // 执行 lifecycleChaincodeInstall 返回的结果
    返回结果：
	
  { "appro_res":true}
	   
	示例参数：
	chaincodeName:fabcarsdkJava
	chaincodeVersion:1
	packageid:fabcarsdkJava_1:a74afc2ce57d1cb29bbd22cb562e46d0479def6d80e9a59aa4e2e1f414aa3418

	使用：
    om.lifecycleChaincodeApproveformyorg(...)
```

3. **像通道提交链码**

```
	 public JSONObject lifecycleChaincodeCommit(
                                                         String chaincodeName,  // 链码名字，同lifecycleChaincodeInstall
                                                         String chaincodeVersion, // 链码版本，同lifecycleChaincodeInstall
                                                         String packageid) // 执行 lifecycleChaincodeInstall 返回的结果
    返回结果：
	
  { "commit_res":true}
  
	示例参数:
	chaincodeName:fabcarsdkJava
	chaincodeVersion:1
	packageid:fabcarsdkJava_1:a74afc2ce57d1cb29bbd22cb562e46d0479def6d80e9a59aa4e2e1f414aa3418
	
	使用：
    om.lifecycleChaincodeCommit(...)
```

4. **Init链码**
```
 public JSONObject lifecycleChaincodeInit(
                                                         String chaincodeName,  // 链码名字，同lifecycleChaincodeInstall
                                                         String chaincodeVersion, // 链码版本，同lifecycleChaincodeInstall
                                                         String packageid, // 执行 lifecycleChaincodeInstall 返回的结果
                                             			 String initFunc) // 链码初始化的init函数名
   返回结果：
   
  { "init":true}
  
	示例参数:
	chaincodeName:fabcarsdkJava
	chaincodeVersion:1
	packageid:fabcarsdkJava_1:a74afc2ce57d1cb29bbd22cb562e46d0479def6d80e9a59aa4e2e1f414aa3418
	initFunc:initLedger
	
	使用：
    om.lifecycleChaincodeInit(...)
```

### 使用链码
------

1.  **invoke** 执行智能合约的写入接口（对账本的状态进行改变）

```
	public JSONObject invoke(
                                              String chaincodeName, // 链码名字
                                              String fcn, // 执行的函数名
                                              String[] args) // json 格式的数组
	
	返回结果：
  {
   "status":200,
   "data":null,
   "txid":"9737452c62299b5c4ddbb0d2dcc82170bfae75ed4599c5d115183d2f5db166a0"
  }
  
	示例参数:
	chaincode:fabcar
	fcn:createCar
	args:["CAR102", "Holden", "Barina", "brown", "zhouzhang"]
	
  使用：
  om.invoke(...)
```
2.  **query** 执行智能合约查询接口（未对账本的状态进行改变）

```
	public JSONObject query(
                                              String chaincodeName, // 链码名字
                                              String fcn, // 执行的函数名
                                              String[] args) // json 格式的数组
	返回结果：
  {
   "status":200,
   "data":"{}",
   "txid":"9737452c62299b5c4ddbb0d2dcc82170bfae75ed4599c5d115183d2f5db166a0"
  }
	示例参数:
	chaincode:fabcar
	fcn:queryCar
	args:["CAR102"]
	
  使用：
  om.query(...)
  
```

### 关于channel

------

1.  **通过区块编号查询区块**

```
	 public JSONObject queryBlockByNumber(long blockNumber)

	示例参数:
	number:10
	
	使用 
	om.queryBlockByNumber(...)
	
```
	
2.  **通过区块HASH查询区块**

```

	 public JSONObject queryBlockByHash(String hash)

	示例参数:
	hash:ae4be3738029c334ce14a86944c53946827b61c310169f98cfac13b09cf90c67

	使用 
	om.queryBlockByHash(...)

```

3.  **通过交易ID查询区块**

```
	public JSONObject queryBlockByTransactionID(String txid)

	示例参数:
	txID:9737452c62299b5c4ddbb0d2dcc82170bfae75ed4599c5d115183d2f5db166a0

	使用 
	om.queryBlockByTransactionID(...)
	
	
```


4.  **获取区块信息**

```

 public JSONObject getBlockchainInfo()

	示例参数: void

	使用 
	om.getBlockchainInfo(...)
	
```
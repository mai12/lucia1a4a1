package cn.flkj.lucia1a4a1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lucia1a4a1Application {

    public static void main(String[] args) {
        SpringApplication.run(Lucia1a4a1Application.class, args);
    }

}

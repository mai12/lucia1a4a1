package cn.flkj.lucia1a4a1.config;


import cn.flkj.lucia1a4a1.intermediate.OrgManager;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.hyperledger.fabric.protos.peer.Query;
import org.hyperledger.fabric.sdk.Peer;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;


/**
 * @author : 州长在手 2020/9/17 下午7:56
 */
@Component
@ConfigurationProperties(prefix = "flkj")
@Data
@Slf4j
public class Runner {

    /*init*/
    private String cc;
    private String orgMSPID;

    private boolean openTLS;
    /*setUser*/
    private String userleagueName;
    private String userorgName;
    private String userpeerName;
    private String username;
    private String userskPath;
    private String usercertificatePath;
    /*addOrderer*/
    private String orderername;
    private String ordererlocation;
    private String ordererserverCrtPath;
    private String ordererclientCertPath;
    private String ordererclientKeyPath;
    /*addPeer*/
    private String peerName;
    private String peerLocation;
    private String peerEventHubLocation;
    private String peerserverCrtPath;
    private String peerclientCertPath;
    private String peerclientKeyPath;
    /*setChannel*/
    private String channelName;


    @Bean(name = "om")
    public OrgManager orgManager() {

        try {

            OrgManager om = new OrgManager(
                    getOrgMSPID(),
                    isOpenTLS());

            om.addOrderer(getOrderername(),
                    getOrdererlocation(),
                    getOrdererserverCrtPath(),
                    getOrdererclientCertPath(),
                    getOrdererclientKeyPath());

            om.addPeer(getPeerName(),
                    getPeerLocation(),
                    getPeerEventHubLocation(),
                    getPeerserverCrtPath(),
                    getPeerclientCertPath(),
                    getPeerclientKeyPath());

            om.setUser(getUserleagueName(),
                    getUserorgName(),
                    getUserpeerName(),
                    getUsername(),
                    getUserskPath(),
                    getUsercertificatePath(),
                    getOrgMSPID());

            om.setChannel(getChannelName());

//            om.addChainCode(
//                    "fabcar",
//                    "",
//                    "",
//                    "",
//                    "1",
//                    20000);
            om.start();
            System.out.println("----------------FM INIT OK !!!----------------");
//           JSONObject jso = om.queryBlockByNumber(20);
//            System.out.println(jso);
//            System.exit(0);
            return om;
        } catch (Exception e) {
            log.error(e.getMessage());
            System.out.println("----------------FM INIT FAILED & EXIT----------------");
            System.exit(0);
            return null;
        }
    }
}

package cn.flkj.lucia1a4a1.controller;


import cn.flkj.lucia1a4a1.intermediate.OrgManager;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * @author : 州长在手 2020/9/10 上午11:52
 * 1. 测试数据加载
 */
@Slf4j
@RestController
@CrossOrigin
@RequestMapping(value = "/sub")
public class SubstrateController {

    private final OrgManager orgManager;

    public SubstrateController(OrgManager orgManager) {
        this.orgManager = orgManager;
    }


    @PostMapping("/invoke")
    public Object invoke(
            @RequestParam("chaincode") String chaincode,
            @RequestParam("fcn") String fcn,
            @RequestParam("args") String args)  {
        return orgManager.invoke(
                chaincode,
                fcn,
                JSONObject.parseArray(args).<String>toArray(new String[0]));
    }

    @PostMapping("/query")
    public Object query(
            @RequestParam("chaincode") String chaincode,
            @RequestParam("fcn") String fcn,
            @RequestParam("args") String args){
        return orgManager.query(
                chaincode,
                fcn,
                JSONObject.parseArray(args).<String>toArray(new String[0]));
    }

    @PostMapping("/queryBlockByHash")
    public Object queryBlockByHash(@RequestParam("hash") String hash){
         return orgManager.queryBlockByHash(hash);
    }

    @PostMapping("/queryBlock")
    public Object queryBlock(@RequestParam("number") long number) {
        System.out.println("number " + number);
        return orgManager.queryBlockByNumber(number);
    }
    // queryBlockByTransactionID
    @PostMapping("/queryBlockByTransactionID")
    public Object queryBlockByTransactionID(@RequestParam("txID") String txID) {
        return orgManager.queryBlockByTransactionID(txID);
    }
    // getBlockchainInfo
    @PostMapping("/getBlockchainInfo")
    public Object getBlockchainInfo() {
        return orgManager.getBlockchainInfo();
    }
    // queryInstantiated
    @PostMapping("/queryInstantiated")
    public Object queryInstantiated() {
        return orgManager.queryInstantiated();
    }

    @PostMapping("/install")
    public Object install(
            @RequestParam("chaincodeName") String chaincodeName,
            @RequestParam("chaincodeVersion") String chaincodeVersion,
            @RequestParam("chaincodeLabel") String chaincodeLabel,
            @RequestParam("chaincodeSourceLocation") String chaincodeSourceLocation,
            @RequestParam("chaincodePath") String chaincodePath,
            @RequestParam("metadadataSource") String metadadataSource,
            @RequestParam("chaincodeEndorsementPolicyPath") String chaincodeEndorsementPolicyPath) {
       try {

            return orgManager.lifecycleChaincodeInstall(chaincodeName,chaincodeVersion,chaincodeLabel,chaincodeSourceLocation,chaincodePath,metadadataSource,chaincodeEndorsementPolicyPath);

        }catch (Exception e){
            System.out.println(e.getMessage());
            return  e.getMessage();
        }
    }

    @PostMapping("/approveformyorg")
    public Object approveformyorg(  @RequestParam("chaincodeName") String chaincodeName,
                                    @RequestParam("chaincodeVersion") String chaincodeVersion,
                                    @RequestParam("packageid") String packageid){
        try {

            return orgManager.lifecycleChaincodeApproveformyorg(chaincodeName,chaincodeVersion,packageid);

        }catch (Exception e){
            System.out.println(e.getMessage());
            return  e.getMessage();
        }

    }

    @PostMapping("/commit")
    public Object commit(  @RequestParam("chaincodeName") String chaincodeName,
                           @RequestParam("chaincodeVersion") String chaincodeVersion,
                           @RequestParam("packageid") String packageid){
        try {
            return orgManager.lifecycleChaincodeCommit(chaincodeName,chaincodeVersion,packageid);
        }catch (Exception e){
            System.out.println(e.getMessage());
            return  e.getMessage();
        }
    }

    @PostMapping("/init")
    public Object init(  @RequestParam("chaincodeName") String chaincodeName,
                         @RequestParam("chaincodeVersion") String chaincodeVersion,
                         @RequestParam("packageid") String packageid,
                         @RequestParam("initFunc") String initFunc,
                         @RequestParam("args") String args){
        try {

            return orgManager.lifecycleChaincodeInit(chaincodeName,chaincodeVersion,packageid,initFunc,JSONObject.parseArray(args).<String>toArray(new String[0]));

        }catch (Exception e){
            System.out.println(e.getMessage());
            return  e.getMessage();
        }
    }



}




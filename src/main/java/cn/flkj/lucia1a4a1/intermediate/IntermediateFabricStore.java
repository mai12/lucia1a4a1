package cn.flkj.lucia1a4a1.intermediate;

import org.hyperledger.fabric.sdk.User;

import java.io.IOException;

/**
 * @author : 州长在手 2021/1/13 上午8:58
 */
public interface IntermediateFabricStore {

    void saveState(String keyForFabricStoreName, User user);
    /**
     * 获取与名称相关的值
     *
     * @param name 名称
     * @return 相关值
     */
    User getState(String name);

    void saveMember( IntermediateUser user) throws IOException;

    /**
     * 用给定的名称获取用户
     *
     * @param name            用户名称（User1）
     * @param mspId           会员id
     * @param skPath          带有节点签名密钥的PEM文件——sk路径
     * @param certificatePath 带有节点的X.509证书的PEM文件——certificate路径
     * @return user 用户
     */
    User getMember(String leagueName, String orgName, String peerName, String name, String mspId, String skPath, String certificatePath) throws IOException;

}

package cn.flkj.lucia1a4a1.intermediate;


import org.hyperledger.fabric.sdk.HFClient;
import org.hyperledger.fabric.sdk.User;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author 州长在手 2020/7/6 下午2:36
 */
public class IntermediateOrg {
    /**
     * 执行SDK的orgName
     */
    private String orgName;
    /**
     * 执行SDK的Fabric用户名
     */
    private String username;
    /**
     * orderer 排序服务器集合
     */
    private List<IntermediateOrderer> orderers = new LinkedList<>();
    /**
     * 当前指定的组织名称，如：Org1MSP
     */
    private String orgMSPID;
    /**
     * peer 排序服务器集合
     */
    private List<IntermediatePeer> peers = new LinkedList<>();
    /**
     * 是否开启TLS访问
     */
    private boolean openTLS;
    /**
     * 频道对象
     */
    private IntermediateChannel channel;
    /**
     * 智能合约对象
     */
    private Map<String, IntermediateChaincodeID> chaincodes = new HashMap<>();
    /**
     * 事件监听
     */
    // private BlockListener blockListener;
    //   private ChaincodeEventListener chaincodeEventListener;
    private HFClient client;
//    private Map<String, User> userMap = new HashMap<>();
    //  private FabricStore fabricStore;

    // FabricStore getFabricStore() {
    //     return fabricStore;
    //  }

    //  void setFabricStore(FabricStore fabricStore) {
    //     this.fabricStore = fabricStore;
    //   }

    /**
     * 设置CA默认请求用户名或指定的带密码参数的请求用户名
     *
     * @param username 用户名
     */
    void setUsername(String username) {
        this.username = username;
    }

    String getUsername() {
        return username;
    }

    /**
     * 新增排序服务器
     */
    void addOrderer(String name, String location, String serverCrtPath, String clientCertPath, String clientKeyPath) {
        orderers.add(new IntermediateOrderer(name, location, serverCrtPath, clientCertPath, clientKeyPath));
    }

    /**
     * 获取排序服务器集合
     */
    List<IntermediateOrderer> getOrderers() {
        return orderers;
    }

    /**
     * 设置会员id信息并将用户状态更新至存储配置对象
     *
     * @param orgMSPID 会员id
     */
    void setOrgMSPID(String orgMSPID) {
        this.orgMSPID = orgMSPID;
    }

    public String getOrgMSPID() {
        return orgMSPID;
    }

    void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getOrgName() {
        return orgName;
    }

    /**
     * 新增节点服务器
     */
    void addPeer(String peerName, String peerLocation, String peerEventHubLocation, String serverCrtPath, String clientCertPath, String clientKeyPath) {
        peers.add(new IntermediatePeer(peerName, peerLocation, peerEventHubLocation, serverCrtPath, clientCertPath, clientKeyPath));
    }

    /**
     * 获取排序服务器集合
     */
    List<IntermediatePeer> getPeers() {
        return peers;
    }

    void setChannel(IntermediateChannel channel) {
        this.channel = channel;
    }

    IntermediateChannel getChannel() {

        return channel;
    }

    void addChaincode(IntermediateChaincodeID chaincode) {
        String ccName = chaincode.getChaincodeName();
        this.chaincodes.put(ccName, chaincode);
    }

    IntermediateChaincodeID getChainCode(String ChaincodeName) {
        return chaincodes.get(ChaincodeName);
    }

    Map<String, IntermediateChaincodeID> getChainCodes() {
        return chaincodes;
    }


    /**
     * 设置是否开启TLS
     *
     * @param openTLS 是否
     */
    void openTLS(boolean openTLS) {
        this.openTLS = openTLS;
    }

    /**
     * 获取是否开启TLS
     *
     * @return 是否
     */
    boolean openTLS() {
        return openTLS;
    }

    void addUser(IntermediateUser user) throws IOException {
        FabricStore.getInstance().saveMember(user);
    }

    User getUser(String keyValStoreName ) {
      return   FabricStore.getInstance().getState(keyValStoreName);
    }

    void setClient(HFClient client) {
        this.client = client;
    }

    HFClient getClient() {
        return client;
    }
}

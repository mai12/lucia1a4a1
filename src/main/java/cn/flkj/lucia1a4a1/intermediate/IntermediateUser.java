package cn.flkj.lucia1a4a1.intermediate;


import org.hyperledger.fabric.sdk.Enrollment;
import org.hyperledger.fabric.sdk.User;

import java.io.Serializable;
import java.util.Set;

/**
 * @author : 州长在手 2021/1/12 下午5:08
 */
public class IntermediateUser implements User, Serializable {
    private static final long serialVersionUID = 8077132186383604355L;
    // 名称
    private final String name;
    // 带有节点签名密钥的PEM文件——sk
    private String skPath;
    // 带有节点的X.509证书的PEM文件——certificate
    private String certificatePath;
    // 角色
    private Set<String> roles;
    // 账户
    private String account;
    // 从属联盟
    private String affiliation;
    // 组织
    private String organization;
    // 注册操作的密钥
    private String enrollmentSecret;
    // 注册登记操作
    private Enrollment enrollment = null;
    // 这个属性不会被序列化。
    // private transient IntermediateFabricStore keyValStore;

    private final String keyForFabricStoreName;

    private String mspId;


    public IntermediateUser(String leagueName, String orgName, String peerName, String name, String skPath, String certificatePath , String mapId) {
        this.name = name;
        this.skPath = skPath;
        this.certificatePath = certificatePath;
        this.mspId = mapId;
        this.keyForFabricStoreName = toKeyValStoreName(leagueName, orgName, peerName, name);
    }

    // --------------------------------------------------------------
    // KV存储时，存储的名字
    public static String toKeyValStoreName(String leagueName, String orgName, String peerName, String name) {
        return String.format("%s%s%s%s", leagueName, orgName, peerName, name);
    }
    // --------------------------------------------------------------
    /**
     *
     */

    public String getKeyForFabricStoreName() {
        return keyForFabricStoreName;
    }

    /**
     * 获取标识用户的名称。
     *
     * @return the user name.
     */
    @Override
    public String getName() {
        return this.name;
    }

    public String getSkPath() {
        return skPath;
    }

    public void setSkPath(String skPath) {
        this.skPath = skPath;
    }

    public String getCertificatePath() {
        return certificatePath;
    }

    public void setCertificatePath(String certificatePath) {
        this.certificatePath = certificatePath;
    }

    /**
     * 获取用户所属的角色。
     *
     * @return role names.
     */
    @Override
    public Set<String> getRoles() {
        return this.roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    /**
     * 获取用户的帐户
     *
     * @return the account name
     */
    @Override
    public String getAccount() {
        return this.account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    /**
     * 获取用户的从属关系。
     *
     * @return the affiliation.
     */
    @Override
    public String getAffiliation() {
        return this.affiliation;
    }

    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }

    /**
     * 获取用户的注册证书信息。
     *
     * @return the Enrollment
     */
    @Override
    public Enrollment getEnrollment() {
        return this.enrollment;
    }

    /**
     * 获取用户组织提供的成员资格服务提供商标识符。
     *
     * @return the MSPID
     */
    @Override
    public String getMspId() {
        return this.mspId;
    }

    public void setMspId(String mspID) {
        this.mspId = mspID;
        FabricStore.getInstance().saveState(keyForFabricStoreName,this);
    }

    /**
     * 确定此名称是否已注册。
     *
     * @return {@code true} if enrolled; otherwise {@code false}.
     */
    public boolean isEnrolled() {
        return this.enrollment != null;
    }

    public String getEnrollmentSecret() {
        return enrollmentSecret;
    }

    public void setEnrollmentSecret(String enrollmentSecret) {
        this.enrollmentSecret = enrollmentSecret;
        FabricStore.getInstance().saveState(keyForFabricStoreName,this);
    }

    public void setEnrollment(Enrollment enrollment) {
        this.enrollment = enrollment;
        FabricStore.getInstance().saveState(keyForFabricStoreName,this);

    }

    public void setIdemixEnrollment(Enrollment enrollment) {
        this.enrollment = enrollment;
    }


}

package cn.flkj.lucia1a4a1.intermediate;

import org.hyperledger.fabric.sdk.Enrollment;

import java.io.Serializable;
import java.security.PrivateKey;

/**
 * 使用此选项可确保SDK不依赖于非身份混合器的HFCA注册
 *
 * @author : 州长在手 2021/1/13 上午9:21
 */
public class IntermediateEnrollment implements Enrollment, Serializable {

    private static final long serialVersionUID = 6965341351799577442L;

    /** 私钥 */
    private final PrivateKey privateKey;
    /** 授权证书 */
    private final String certificate;

    public IntermediateEnrollment(PrivateKey privateKey, String certificate) {
        this.certificate = certificate;
        this.privateKey = privateKey;
    }

    @Override
    public PrivateKey getKey() {
        return privateKey;
    }

    @Override
    public String getCert() {
        return certificate;
    }
}

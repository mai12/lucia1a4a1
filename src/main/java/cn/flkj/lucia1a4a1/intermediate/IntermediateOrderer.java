package cn.flkj.lucia1a4a1.intermediate;

/**
 * 描述：中继Orderer排序服务对象
 *
 * @author : 州长在手 2021/1/5 上午9:41
 */
public class IntermediateOrderer {
    /** orderer 排序服务器的域名 */
    private String ordererName;
    /** orderer 排序服务器的访问地址 */
    private String ordererLocation;
    /** tls请求证书 */
    private String serverCrtPath;
    private String clientCertPath;
    private String clientKeyPath;

   public IntermediateOrderer(String ordererName, String ordererLocation, String serverCrtPath, String clientCertPath, String clientKeyPath) {
        super();
        this.ordererName = ordererName;
        this.ordererLocation = ordererLocation;
        this.serverCrtPath = serverCrtPath;
        this.clientCertPath = clientCertPath;
        this.clientKeyPath = clientKeyPath;
    }

    String getOrdererName() {
        return ordererName;
    }

    void setOrdererLocation(String ordererLocation) {
        this.ordererLocation = ordererLocation;
    }

    String getOrdererLocation() {
        return ordererLocation;
    }

    String getServerCrtPath() {
        return serverCrtPath;
    }

    public String getClientCertPath() {
        return clientCertPath;
    }

    public String getClientKeyPath() {
        return clientKeyPath;
    }
}
